import * as BABYLON from 'babylonjs';

export default class ExplodeScene {
  private canvas: HTMLCanvasElement;
  private engine: BABYLON.Engine;
  private scene: BABYLON.Scene;
  private camera: BABYLON.ArcRotateCamera;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.engine = new BABYLON.Engine(this.canvas, true);

    this.scene = new BABYLON.Scene(this.engine);
    this.scene.useRightHandedSystem = true; // WTF Babylon

    this.camera = new BABYLON.ArcRotateCamera(
      'camera1',
      0, // yaw
      0, // pitch
      10, // radius
      new BABYLON.Vector3(0, 0, 0),
      this.scene
    );
    this.camera.setPosition(new BABYLON.Vector3(0, 3, 10));
    this.camera.lowerRadiusLimit = 1;
    this.camera.attachControl(this.canvas, false);

    this.buildScene();

    this.engine.runRenderLoop(() => {
      this.scene.render();
    });
  }

  public resize(): void {
    this.engine.resize();
  }

  private buildScene(): void {
    const light = new BABYLON.HemisphericLight(
      'light1',
      new BABYLON.Vector3(1, 2, 3),
      this.scene
    );

    const sphere = BABYLON.MeshBuilder.CreateSphere(
      'sphere',
      { segments: 16, diameter: 2 },
      this.scene
    );
    sphere.position.y = 1;
    sphere.convertToFlatShadedMesh();
    this.setMeshColors(sphere);

    const ground = BABYLON.MeshBuilder.CreateGround(
      'ground',
      { width: 6, height: 6, subdivisions: 10 },
      this.scene
    );
    this.setMeshColors(ground);
  }

  private setMeshColors(mesh: BABYLON.Mesh): void {
    const normals = mesh.getVerticesData(BABYLON.VertexBuffer.NormalKind);
    if (normals) {
      const colors = new Float32Array((normals.length / 3) * 4);
      const numVerts = normals.length / 3;

      for (let vi = 0; vi < numVerts; ++vi) {
        const normal = new BABYLON.Vector3(
          normals[vi * 3 + 0],
          normals[vi * 3 + 1],
          normals[vi * 3 + 2]
        );
        normal.scaleInPlace(0.5);
        normal.addInPlace(new BABYLON.Vector3(0.5, 0.5, 0.5));

        colors[vi * 4 + 0] = normal.x;
        colors[vi * 4 + 1] = normal.y;
        colors[vi * 4 + 2] = normal.z;
        colors[vi * 4 + 3] = 1.0;
      }

      mesh.setVerticesData(BABYLON.VertexBuffer.ColorKind, colors);
    }
  }
}
