import * as BABYLON from 'babylonjs';
import explodeVert from 'ts-shader-loader!@/assets/shaders/explode.vert';
import explodeFrag from 'ts-shader-loader!@/assets/shaders/explode.frag';

export default class ExplodeScene {
  private canvas: HTMLCanvasElement;
  private engine: BABYLON.Engine;
  private scene: BABYLON.Scene;
  private camera: BABYLON.ArcRotateCamera;
  private material: BABYLON.ShaderMaterial;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.engine = new BABYLON.Engine(this.canvas, true);

    this.scene = new BABYLON.Scene(this.engine);
    this.scene.useRightHandedSystem = true; // WTF Babylon

    this.camera = new BABYLON.ArcRotateCamera(
      'camera1',
      0, // yaw
      0, // pitch
      10, // radius
      new BABYLON.Vector3(0, 0, 0),
      this.scene
    );
    this.camera.setPosition(new BABYLON.Vector3(0, 7, 25));
    this.camera.lowerRadiusLimit = 1;
    this.camera.attachControl(this.canvas, false);

    this.material = this.createShaderMaterial();

    this.buildScene();

    this.engine.runRenderLoop(() => {
      this.scene.render();
    });
  }

  set explodeScale(scale: number) {
    this.material.setFloat('explodeScale', scale);
  }

  public resize(): void {
    this.engine.resize();
  }

  private createShaderMaterial(): BABYLON.ShaderMaterial {
    BABYLON.Effect.ShadersStore.customVertexShader = explodeVert;
    BABYLON.Effect.ShadersStore.customFragmentShader = explodeFrag;

    const material = new BABYLON.ShaderMaterial(
      'shader',
      this.scene,
      {
        vertex: 'custom',
        fragment: 'custom',
      },
      {
        attributes: ['position', 'normal', 'color'],
        uniforms: ['worldViewProjection', 'explodeScale', 'fadeColor'],
      }
    );
    material.backFaceCulling = false;
    material.setColor4('fadeColor', this.scene.clearColor);
    material.setFloat('explodeRotationMultiplier', 7.0);
    material.setFloat('explodeDistance', 7.0);
    return material;
  }

  private buildScene(): void {
    const sphere = BABYLON.MeshBuilder.CreateSphere(
      'sphere',
      { segments: 16, diameter: 2 },
      this.scene
    );
    sphere.position.y = 1;
    this.setMeshColorsAndMaterial(sphere);

    const box = BABYLON.MeshBuilder.CreateBox('box', { size: 3 }, this.scene);
    box.position.x = 3;
    box.position.y = 1.5;
    this.setMeshColorsAndMaterial(box);

    const ground = BABYLON.MeshBuilder.CreateGround(
      'ground',
      { width: 12, height: 12, subdivisions: 10 },
      this.scene
    );
    this.setMeshColorsAndMaterial(ground);

    const mesh = new BABYLON.Mesh('custom', this.scene);

    const vertexData = new BABYLON.VertexData();
    vertexData.positions = [-3, 0, 0, -1, 0, 0, -2, 2, 0];
    vertexData.normals = [0, 0, 1, 0, 0, 1, 0, 0, 1];
    vertexData.indices = [0, 1, 2];

    vertexData.applyToMesh(mesh, true);
    mesh.toLeftHanded();
    mesh.position.x = -1;
    this.setMeshColorsAndMaterial(mesh);
  }

  private setMeshColorsAndMaterial(mesh: BABYLON.Mesh): void {
    mesh.material = this.material;
    mesh.convertToFlatShadedMesh();
    mesh.useVertexColors = true;

    const positions = mesh.getVerticesData(BABYLON.VertexBuffer.PositionKind);

    if (positions) {
      const numVerts = positions.length / 3;
      const centers = new Float32Array(numVerts * 4);

      // iterate 3 verts at a time (one triangle)
      for (let i = 0; i < numVerts; i += 3) {
        const x =
          (positions[(i + 0) * 3 + 0] +
            positions[(i + 1) * 3 + 0] +
            positions[(i + 2) * 3 + 0]) /
          3;
        const y =
          (positions[(i + 0) * 3 + 1] +
            positions[(i + 1) * 3 + 1] +
            positions[(i + 2) * 3 + 1]) /
          3;
        const z =
          (positions[(i + 0) * 3 + 2] +
            positions[(i + 1) * 3 + 2] +
            positions[(i + 2) * 3 + 2]) /
          3;

        centers[(i + 0) * 4 + 0] = x;
        centers[(i + 0) * 4 + 1] = y;
        centers[(i + 0) * 4 + 2] = z;
        centers[(i + 0) * 4 + 3] = 1;

        centers[(i + 1) * 4 + 0] = x;
        centers[(i + 1) * 4 + 1] = y;
        centers[(i + 1) * 4 + 2] = z;
        centers[(i + 1) * 4 + 3] = 1;

        centers[(i + 2) * 4 + 0] = x;
        centers[(i + 2) * 4 + 1] = y;
        centers[(i + 2) * 4 + 2] = z;
        centers[(i + 2) * 4 + 3] = 1;
      }

      mesh.setVerticesData(BABYLON.VertexBuffer.ColorKind, centers);
    }
  }
}
