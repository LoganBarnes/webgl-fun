precision highp float;
precision highp int;

const vec3 globalColor = vec3(1.0, 0.5, 0.1);

varying vec3 vNormal;

uniform float explodeScale;
uniform vec4 fadeColor;

void main() {
  vec3 normal = normalize(gl_FrontFacing ? vNormal : -vNormal);
  vec3 normalColor = normal * 0.5 + 0.5;
  float a = pow(explodeScale, 2.0);
  vec3 color = mix(normalColor, fadeColor.rgb, a);
  gl_FragColor = vec4(color, 1.0);
}
