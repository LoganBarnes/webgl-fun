precision highp float;
precision highp int;

attribute vec3 position;
attribute vec3 normal;
attribute vec4 color;

uniform mat4 worldViewProjection;
uniform float explodeScale;
uniform float explodeRotationMultiplier;
uniform float explodeDistance;

varying vec3 vNormal;

/*
 * https://stackoverflow.com/a/4275343
 */
float rand(in vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

/*
 * Assumes normalized axis
 */
mat3 rotationMatrix(in vec3 axis, in float angle) {
  float s = sin(angle);
  float c = cos(angle);
  float oc = 1.0 - c;

  return mat3(
    oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
    oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
    oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c
  );
}

void main() {
  vec3 triangleCenter = color.xyz;

  vec3 rotationAxis;
  rotationAxis.x = rand(color.zw) * 2.0 - 1.0;
  rotationAxis.y = rand(color.xy) * 2.0 - 1.0;
  rotationAxis.z = rand(rotationAxis.xy) * 2.0 - 1.0;
  rotationAxis = normalize(rotationAxis);

  float rotationAngle = rand(rotationAxis.yz) * 2.0 - 1.0;
  rotationAngle *= explodeScale * explodeRotationMultiplier;

  mat3 rotation = rotationMatrix(rotationAxis, rotationAngle);


  // translate to origin
  vec3 offset = position - triangleCenter;
  // rotate
  offset = rotation * offset;

  vNormal = rotation * normal;
  vec3 explodeDirection = normalize(triangleCenter + normal);

  vec3 explodedPosition = triangleCenter + explodeDirection * explodeScale * explodeDistance + offset;
  gl_Position   = worldViewProjection * vec4(explodedPosition, 1.0);
}
