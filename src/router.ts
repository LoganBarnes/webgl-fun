import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/explode',
      name: 'explode',
      // route level code-splitting
      // this generates a separate chunk (explode.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "explode" */ './views/Explode.vue'),
    },
    {
      path: '/stock',
      name: 'stock',
      component: () =>
        import(/* webpackChunkName: "stock" */ './views/Stock.vue'),
    },
  ],
});
