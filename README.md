# WebGL Fun

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Lints and fixes files

```
yarn run lint
```

### Formats files

```
yarn run format
```

## Live Website

https://loganbarnes.gitlab.io/webgl-fun
